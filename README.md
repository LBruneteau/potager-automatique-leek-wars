# Potager automatique leek wars  
  
Vous aimez *Leek Wars* mais vous trouvez que les combats sont trop longs?  
Ce programme est fait pour vous !  
Il lancera automatiquement le nombre de combats désirés face à l'adversaire le plus faible, et vous retournera le résultat

### Comment lancer le programme ?
 
Tout d'abord il faudra **télécharger le fichier**  
Le module **requests** sera nécessaire au fonctionnement du script  
`pip install requests`  
Puis le lancer avec python3 `python3 /chemin/vers/potager.py`

### Comment utiliser le programme ?

Entrez tout d'abord le **nombre de combat** que vous voulez faire  
Ensuite tapez votre **identifiant** puis votre **mot de passe** leek wars  
Il faudra finalement entrer l'**id** de votre poireau  
On peut la trouver sur la page de votre poireau (https://leekwars.com/ID)  
Le programme se charge du reste ! 

###### Remerciements

Merci à *lapinrose* , sur le forum *leek wars* de m'avoir aidé pour les headers