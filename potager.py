#POTAGER SOLO LEEK WARS

#Importations
import requests
import json
from random import randint
import time as t

#Entrée
nb_combats:int = int(input('Combien de combats voulez-vous faire ? : '))
login:str = input('Entrez votre indentifiant Leek Wars : ')
mdp:str = input('Quel est votre mot de passe ? : ')
ID:str = input('Quel est l\'ID de votre poireau ? : ')

#Traitement

#Connexion
r = requests.post('https://leekwars.com/api/farmer/login-token' , data={'login':login , 'password':mdp})
token = r.json()['token']
header = {'Content-Type':'application/json' , 'Authorization':'Bearer {}'.format(token) , 'Cookie':'PHPSESSID={}'.format(randint(0,1000))}

#Combats
for _ in range(nb_combats):
    #liste des ennemis disponibles dans le potager
    r = requests.get('https://leekwars.com/api/garden/get-leek-opponents/{}'.format(ID) , headers=header)
    les_ennemis = r.json()['opponents']

    #Obtention de l'ID du poireau avec le moins de talent
    id_ennemis:list = [k['id'] for k in les_ennemis]
    talent_ennemis:list = [['talent' for k in les_ennemis]]
    index_min:int = talent_ennemis.index(min(talent_ennemis))
    son_id:int = id_ennemis[index_min]

    #Lancer le combat
    r = requests.get('https://leekwars.com/api/garden/start-solo-fight/{}/{}'.format(ID , son_id) , headers=header)
    
    #Permet de laisser le combat se générer et de ne pas surcharger les serveurs. NE PAS RÉDUIRE CE TEMPS !!!!!
    t.sleep(5)

    #Résultat du combat
    id_combat:int = r.json()['fight']
    r = requests.get('https://leekwars.com/api/fight/get/{}'.format(id_combat) , headers=header)
    combat = r.json()['fight']
    gagnant = combat['winner']
    leek1 = str(combat['leeks1'][0]['id'])
    if leek1 == ID:
        moi = 1
    else:
        moi = 2
    if gagnant == moi:
        print('Gagné !')
    else:
        print('Perdu...')
print('Terminé')